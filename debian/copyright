This package was debianized by Fabio Brito <fabio@debian-ba.org> on
Fri, 15 Jul 2005 16:30:29 -0300.
It was brought up to date by Holger Levsen <debian@layer-acht.org> on
Sat, 21 Jan 2006 17:58:35 +0000

--------------------------------------------------------------------------------------------

Source: https://github.com/tux4kids/tuxtype/tags

Upstream-Contact: Sam Hart <hart@geekcomix.com>, Jesse D. Andrews <jdandr2@uky.edu>,
		  David Bruce <dbruce@tampabay.rr.com> and others


Copyright: 2001-2003 Tux4Kids  http://www.tux4kids.org/

Tux Typing is licensed under the GNU Public License version 2.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation in version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License with
the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-2;
if not, write to the Free Software Foundation, Inc.,  51 Franklin St, 
Fifth Floor, Boston, MA  02110-1301  USA.

On Debian systems, the complete text of the GNU General Public
License, version 2, can be found in /usr/share/common-licenses/GPL-2.

--------------------------------------------------------------------------------------------

the following is a verbose copy of the contents of
/usr/share/tuxtype/data/images/README_IMAGES.TXT and applies to the
files in /usr/share/tuxtype/data/images/ provided by the tuxtype-data
package:

About The Images Used In This Game
----------------------------------

The majority of the graphics used in this title were made
exclusively by myself (Sam Hart, <hart@geekcomix.com>) and I
place under the Gnu GPL (as dictated by the COPYING file in the
root directory of this archive). These images were created
either natively in the GIMP (http://www.gimp.org/) or were hand
drawn, scanned, and manipulated via the GIMP.

Tux was originally drawn by Larry Ewing <ltlewing@isc.tamu.edu>. 
Certain images used in this game were modified from his original 
Tux images found at http://www.isc.tamu.edu/~lewing/linux/

The remainder of the images, are from a number of public domain
sources. Specifically, they are from the National Oceanic and
Atmospheric Administration (NOAA) and the U.S. Fish and Wildlife
Service (FWS). The archives for these services were accessed
through the GIMP-Savvy web-site at

http://gimp-savvy.com/PHOTO-ARCHIVE/

Those images which were obtained from either of these US
government agencies were:

kcas1_1.gif
kcas1_2.gif
kcas1_3.gif
kcas1_4.gif
kcas2_1.gif
kcas2_2.gif
kcas2_3.gif
kcas2_4.gif
kcas3_1.gif
kcas3_2.gif
kcas3_3.gif
kcas3_4.gif

wcas1_1.gif
wcas1_2.gif
wcas1_3.gif
wcas1_4.gif
wcas2_1.gif
wcas2_2.gif
wcas2_3.gif
wcas2_4.gif
wcas3_1.gif
wcas3_2.gif
wcas3_3.gif
wcas3_4.gif

I'm pretty sure that covers everything that may potentially
cause problems... but just in case I left something out, I can
assure you that any image used in this game was freely
distributable to the best of my knowledge.

Thx.

(PS: As a reward for those of you that read these readme files,
you may notice a file called "hidden.gif" in this directory. The
image is royalty free from same places as *cas*.gif files above
are. But what's significant about it is that it's a special hidden
background level. Simply use "tuxtype --hidden" to use it as the
default background for level 4 on each difficulty level! Enjoy! ;)

--------------------------------------------------------------------------------------------

the following is a verbose copy of the contents of
/usr/share/tuxtype/data/sounds/README_SOUNDS.TXT and applies to the
files in /usr/share/tuxtype/data/sounds/ provided by the tuxtype-data
package:

About The Sound Files Used In This Game
---------------------------------------

Some of the sound files used in this game were made exclusively by myself (Sam
Hart, <hart@geekcomix.com>) and I place under the Gnu GPL as dictated by the
COPYING file in the root directory of this archive. Those files are:

excuseme.wav
bite.wav
run.wav
splat.wav

The following files are taken from tuxmath and where made by Bill Kendrick
<bill@newbreedsoftware.com> - the files are also place under the GPL:

game.mod
game2.mod
game3.mod
laser.wav
pause.wav
tock.wav
explosion.wav  
alarm.wav
buzz.wav
lose.wav
pop.wav
shieldsdown.wav

Sam got the following two files from a CD with copyright-free stuff ("they were
sounds you could legally use to sample, remix, use elsewhere, without having to
get permission or any acknowledgement back to the author"):

win.wav          
winfinal.wav


Emmett Plant (emmett@sonivius.com) is the author of tuxi.ogg and released it in the public domain.

This game is under the Gnu GPL (again, see COPYING) and is open-source. While
this does not preclude it from being included in a commercial packade (such as a
Linux distro) this software is required to be freely distributable. 

If you have any further questions (or you feel a sound file used in this
open-source game has a copyright conflicting enough to not be included
with a Gnu GPL software title) please contact me at hart@geekcomix.com

Thx.

-----------------------------------------

10 Sep 2009

ADDENDUM - the previously present files with license problems (kmus1-4.wav, click.wav) have all been removed
and replaced with 100% free and redistributable alternatives.  I believe the sound files in Tux Typing are
now fully compliant with the Debian Free Software Guidelines.

David Bruce
<davidstuartbruce@gmail.com>

--------------------------------------------------------------------------------------------

ConvertUTF.c/.h are from Unicode, Inc. and are released under a very free (BSD-like) license:

   "Unicode, Inc. hereby grants the right to freely use the information
   supplied in this file in the creation of products supporting the
   Unicode Standard, and to make copies of this file in any form
   for internal or external distribution as long as this notice
   remains attached."

--------------------------------------------------------------------------------------------

/data/fonts/AndikaDesRevG.ttf and /data/fonts/DoulosSILR.ttf come with the following licence 
statement: 

Copyright:

This Font Software is Copyright (c) 1994-2007, SIL International (http://scripts.sil.org/).
All Rights Reserved.

"Doulos SIL" is a Reserved Font Name for this Font Software.
"SIL" is a Reserved Font Name for this Font Software.

This Font Software is licensed under the SIL Open Font License, Version 1.0.
No modification of the license is permitted, only verbatim copy is allowed.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL


-----------------------------------------------------------
SIL OPEN FONT LICENSE Version 1.0 - 22 November 2005
-----------------------------------------------------------

PREAMBLE
The goals of the Open Font License (OFL) are to stimulate worldwide
development of cooperative font projects, to support the font creation
efforts of academic and linguistic communities, and to provide an open
framework in which fonts may be shared and improved in partnership with
others.

The OFL allows the licensed fonts to be used, studied, modified and
redistributed freely as long as they are not sold by themselves. The
fonts, including any derivative works, can be bundled, embedded, 
redistributed and sold with any software provided that the font
names of derivative works are changed. The fonts and derivatives,
however, cannot be released under any other type of license.

DEFINITIONS
"Font Software" refers to any and all of the following:
        - font files
        - data files
        - source code
        - build scripts
        - documentation

"Reserved Font Name" refers to the Font Software name as seen by
users and any other names as specified after the copyright statement.

"Standard Version" refers to the collection of Font Software
components as distributed by the Copyright Holder.

"Modified Version" refers to any derivative font software made by
adding to, deleting, or substituting -- in part or in whole --
any of the components of the Standard Version, by changing formats
or by porting the Font Software to a new environment.

"Author" refers to any designer, engineer, programmer, technical
writer or other person who contributed to the Font Software.

PERMISSION & CONDITIONS
Permission is hereby granted, free of charge, to any person obtaining
a copy of the Font Software, to use, study, copy, merge, embed, modify,
redistribute, and sell modified and unmodified copies of the Font
Software, subject to the following conditions:

1) Neither the Font Software nor any of its individual components,
in Standard or Modified Versions, may be sold by itself.

2) Standard or Modified Versions of the Font Software may be bundled,
redistributed and sold with any software, provided that each copy
contains the above copyright notice and this license. These can be
included either as stand-alone text files, human-readable headers or
in the appropriate machine-readable metadata fields within text or
binary files as long as those fields can be easily viewed by the user.

3) No Modified Version of the Font Software may use the Reserved Font
Name(s), in part or in whole, unless explicit written permission is
granted by the Copyright Holder. This restriction applies to all 
references stored in the Font Software, such as the font menu name and
other font description fields, which are used to differentiate the
font from others.

4) The name(s) of the Copyright Holder or the Author(s) of the Font
Software shall not be used to promote, endorse or advertise any
Modified Version, except to acknowledge the contribution(s) of the
Copyright Holder and the Author(s) or with their explicit written
permission.

5) The Font Software, modified or unmodified, in part or in whole,
must be distributed using this license, and may not be distributed
under any other license.

TERMINATION
This license becomes null and void if any of the above conditions are
not met.

DISCLAIMER
THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
OTHER DEALINGS IN THE FONT SOFTWARE.

--------------------------------------------------------------------------------------------

/data/fonts/Dachana_w01.ttf comes with the following licence statement: 

Uptream Authors:
        Baiju M <baijum81@lycos.com>
        Kevin & Siji <http://kevinsiji.goldeye.info>
        Rachana Akshara Vedi <http://www.rachanamalayalam.org>

Copyright:

        (MalOtf.ttf)
        Copyright (C) Jeroen Hellingman <jehe@kabelfoon.nl>,
                      N.V Shaji <nvshaji@yahoo.com>

        (AnjaliOldLipi-0.730.ttf)
        Copyright (C) 2004 Kevin & Siji

        (Rachana_w01.ttf)
        Copyright (C) 2005 Rachana Akshara Vedi <http://www.rachanamalayalam.org>

License:

        Rachana is a language campaign forum for the Original script of Malayalam
        in the digital computing. It started in 1999 under the leadership of
        R. Chitrajakumar (Malayalam Lexicon, Kerala University, Palayalm,
        Thiruvananthapuram). Hussain KH (KFRI, Kerala Forest Research Institute,
        Peechi, Thrissur) is the project coordinator who designed the font.
        Other Members are:  Gangadharan N (Malayalam Lexicon) who assisted
        Chitrajakumar in compiling the exhaustive character set of Malayalam;
        Dr. P Vijayakumaran Nair (KFRI) who assisted in programming the Rachana
        text editor which was used by many from 1999 to 2005 to typeset Books
        and journal articles in the old Lipi; Subash Kuriakose (KFRI) who gave
        advice to Hussain in the many aspects of Malayalam calligraphy. Later in
        2004 Rajeev J Sebastian joined the Rachana team, whose mastery in Linux
        and untiring work is elevating the campaign to materialize its highest
        dream that to embed the original script in the OS enabling it in the
        whole field of digital applications. Dr. Mammen Chundamannil (KFRI) is
        an ardent supporter and well wisher of Rachana whose article 'For our
        language, our script' has spread the vision and objectives of Rachana to
        the whole world.

        There seems to be some confusion regarding the license of Rachana.
        Praveen has confirmed that Rachana is indeed under GPL. Any queries
        regarding the exact status of Rachana license issue should be directed
        to Praveen <pravi.a@gmail.com>

        This package is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.

        This package is distributed in the hope that it will be useful, but
        WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this package; if not, write to the Free Software Foundation,
        Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General Public License can be
found in /usr/share/common-licenses/GPL-2 and /usr/share/common-licenses/GPL.

--------------------------------------------------------------------------------------------

